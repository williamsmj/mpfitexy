mpfitexy
========

Straight line fitting with IDL and MPFIT. 

This is the repository and public version history of MPFITEXY. For installation instructions and a complete description geared towards users see 
[my academic website](http://purl.org/mike/mpfitexy).
